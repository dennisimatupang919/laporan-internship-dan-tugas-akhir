\contentsline {chapter}{COVER}{i}% 
\contentsline {chapter}{LEMBAR PENGESAHAN LAPORAN}{ii}% 
\contentsline {chapter}{LEMBAR PERNYATAAN}{iii}% 
\contentsline {chapter}{HALAMAN PERNYATAAN}{iv}% 
\contentsline {chapter}{ABSTRAK}{v}% 
\contentsline {chapter}{\textit {ABSTRACT}}{vi}% 
\contentsline {chapter}{KATA PENGANTAR}{vii}% 
\contentsline {chapter}{DAFTAR ISI}{viii}% 
\contentsline {chapter}{DAFTAR GAMBAR}{xii}% 
\contentsline {chapter}{DAFTAR TABEL}{xiii}% 
\contentsline {chapter}{DAFTAR SIMBOL}{xiv}% 
\contentsline {chapter}{DAFTAR SINGKATAN}{xv}% 
\contentsline {chapter}{\numberline {I}PENDAHULUAN}{1}% 
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}% 
\contentsline {paragraph}{}{1}% 
\contentsline {paragraph}{}{1}% 
\contentsline {paragraph}{}{1}% 
\contentsline {section}{\numberline {1.2}Identifikasi Masalah}{2}% 
\contentsline {paragraph}{}{2}% 
\contentsline {section}{\numberline {1.3}Tujuan dan Manfaat}{2}% 
\contentsline {subsection}{\numberline {1.3.1}Tujuan}{2}% 
\contentsline {paragraph}{}{2}% 
\contentsline {subsection}{\numberline {1.3.2}Manfaat}{2}% 
\contentsline {paragraph}{}{2}% 
\contentsline {section}{\numberline {1.4}Ruang Lingkup}{3}% 
\contentsline {paragraph}{}{3}% 
\contentsline {chapter}{\numberline {II}LANDASAN TEORI}{4}% 
\contentsline {section}{\numberline {2.1}Deskripsi Topik Yang Sama}{4}% 
\contentsline {subsection}{\numberline {2.1.1}Pencarian Informasi Wisata Daerah Bali menggunakan Teknologi Chatbot}{4}% 
\contentsline {paragraph}{}{4}% 
\contentsline {subsection}{\numberline {2.1.2}Perancangan Chatbot Pusat Informasi Mahasiswa Menggunakan Aiml Sebagai Virtual Assistant Berbasis Web}{4}% 
\contentsline {paragraph}{}{4}% 
\contentsline {subsection}{\numberline {2.1.3}Penggunaan Named Entity Recognition dan Artificial Intelligence Markup Language untuk Penerapan Chatbot Berbasis Teks}{4}% 
\contentsline {paragraph}{}{4}% 
\contentsline {section}{\numberline {2.2}Deskripsi Metode Yang Sama}{5}% 
\contentsline {subsection}{\numberline {2.2.1}Implementasi Natural Language Processing pada Chatbot Peribahasa Indonesia}{5}% 
\contentsline {paragraph}{}{5}% 
\contentsline {subsection}{\numberline {2.2.2}\textit {Comparative study of document similarity algorithms and clustering algorithms for sentiment analysis}}{5}% 
\contentsline {paragraph}{}{5}% 
\contentsline {subsection}{\numberline {2.2.3}Penerapan Algoritma Cosine Similarity dan Pembobotan TF-IDF pada Sistem Klasifikasi Dokumen Skripsi}{6}% 
\contentsline {paragraph}{}{6}% 
\contentsline {subsection}{\numberline {2.2.4}\textit {Cosine similarity to determine similarity measure: Study case in online essay assessment}}{6}% 
\contentsline {paragraph}{}{6}% 
\contentsline {subsection}{\numberline {2.2.5}Perbandingan Metode Cosine Similarity Dengan Metode Jaccard Similarity Pada Aplikasi Pencarian Terjemah Al-Qur\IeC {\textquoteright }an Dalam Bahasa Indonesia}{7}% 
\contentsline {paragraph}{}{7}% 
\contentsline {subsection}{\numberline {2.2.6}Super Agent Chatbot \IeC {\textquotedblleft }3S\IeC {\textquotedblright } Sebagai Media Informasi Menggunakan Metoda Natural Language Processing (NLP)}{7}% 
\contentsline {paragraph}{}{7}% 
\contentsline {section}{\numberline {2.3}Teori Pendukung Penelitian}{7}% 
\contentsline {subsection}{\numberline {2.3.1}\textit {Artificial Intelligence}}{7}% 
\contentsline {paragraph}{}{7}% 
\contentsline {subsection}{\numberline {2.3.2}\textit {Natural Language Processing}}{8}% 
\contentsline {paragraph}{}{8}% 
\contentsline {paragraph}{}{8}% 
\contentsline {subsection}{\numberline {2.3.3}\textit {Chatbot}}{8}% 
\contentsline {paragraph}{}{8}% 
\contentsline {subsection}{\numberline {2.3.4}Algoritma Cosine Similarity}{9}% 
\contentsline {paragraph}{}{9}% 
\contentsline {subsection}{\numberline {2.3.5}\textit {Text Mining}}{9}% 
\contentsline {paragraph}{}{9}% 
\contentsline {subsection}{\numberline {2.3.6}\textit {Webservice}}{10}% 
\contentsline {paragraph}{}{10}% 
\contentsline {subsubsection}{\numberline {2.3.6.1}\textit {Representational State Transfer (REST)}}{10}% 
\contentsline {paragraph}{}{10}% 
\contentsline {subsection}{\numberline {2.3.7}\textit {Black Box Testing}}{11}% 
\contentsline {paragraph}{}{11}% 
\contentsline {chapter}{\numberline {III}GAMBARAN OBYEK STUDY}{12}% 
\contentsline {section}{\numberline {3.1}Obyek Study}{12}% 
\contentsline {paragraph}{}{12}% 
\contentsline {section}{\numberline {3.2}Jenis dan Sumber Data}{12}% 
\contentsline {paragraph}{}{12}% 
\contentsline {subsection}{\numberline {3.2.1}Data Primer}{13}% 
\contentsline {paragraph}{}{13}% 
\contentsline {chapter}{\numberline {IV}METODOLOGI PENELITIAN}{14}% 
\contentsline {section}{\numberline {4.1}Diagram Alur Metodologi Penelitian}{14}% 
\contentsline {paragraph}{}{14}% 
\contentsline {subsection}{\numberline {4.1.1}Pengumpulan Data}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {subsection}{\numberline {4.1.2}Studi Literatur}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {subsection}{\numberline {4.1.3}Perancangan dan Implementasi Sistem}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {subsection}{\numberline {4.1.4}Pengujian Algoritma}{16}% 
\contentsline {paragraph}{}{16}% 
\contentsline {subsection}{\numberline {4.1.5}Kesimpulan}{16}% 
\contentsline {paragraph}{}{16}% 
\contentsline {section}{\numberline {4.2}Tahapan - Tahapan Diagram Metodologi Penelitian 1}{16}% 
\contentsline {paragraph}{}{16}% 
\contentsline {section}{\numberline {4.3}Tahapan - Tahapan Diagram Metodologi Penelitian 2}{17}% 
\contentsline {paragraph}{}{17}% 
\contentsline {chapter}{\numberline {V}PENGUJIAN DAN HASIL}{20}% 
\contentsline {section}{\numberline {5.1}Pengujian}{20}% 
\contentsline {subsection}{\numberline {5.1.1}Analisis Sistem}{20}% 
\contentsline {paragraph}{}{20}% 
\contentsline {subsubsection}{\numberline {5.1.1.1}Analisis Sistem yang Sedang Berjalan}{20}% 
\contentsline {paragraph}{}{20}% 
\contentsline {subsubsection}{\numberline {5.1.1.2}Analisis Sistem yang Akan di Bangun}{20}% 
\contentsline {paragraph}{}{20}% 
\contentsline {subsubsection}{\numberline {5.1.1.3}Analisis Kebutuhan Aplikasi}{21}% 
\contentsline {paragraph}{}{21}% 
\contentsline {subsubsection}{\numberline {5.1.1.4}Perancangan Basis Data}{21}% 
\contentsline {paragraph}{}{21}% 
\contentsline {subsection}{\numberline {5.1.2}Implementasi dan Pengujian Sistem}{23}% 
\contentsline {paragraph}{}{23}% 
\contentsline {subsection}{\numberline {5.1.3}Pengujian Algoritma}{25}% 
\contentsline {paragraph}{}{25}% 
\contentsline {section}{\numberline {5.2}Hasil}{30}% 
\contentsline {paragraph}{}{30}% 
\contentsline {chapter}{\numberline {VI}KESIMPULAN}{32}% 
\contentsline {section}{\numberline {6.1}Kesimpulan Masalah}{32}% 
\contentsline {paragraph}{}{32}% 
\contentsline {section}{\numberline {6.2}Kesimpulan Metode}{32}% 
\contentsline {paragraph}{}{32}% 
\contentsline {section}{\numberline {6.3}Kesimpulan Penelitian}{33}% 
\contentsline {paragraph}{}{33}% 
\contentsline {chapter}{\numberline {VII} DISKUSI}{34}% 
\contentsline {section}{\numberline {7.1}Diskusi}{34}% 
\contentsline {paragraph}{}{34}% 
\contentsline {section}{\numberline {7.2}Rekomendasi}{34}% 
\contentsline {paragraph}{}{34}% 
\contentsline {chapter}{Daftar Pustaka}{35}% 
\contentsline {chapter}{\numberline {A}Sumber Data}{39}% 
\contentsline {chapter}{\numberline {B}Source Code}{40}% 
\contentsline {chapter}{\numberline {C}Curriculum Vitae}{41}% 
